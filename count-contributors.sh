#!/bin/bash
set -e -o pipefail
trap 'rm ${tmpfile}' exit   # Make sure tmpfile is cleaned up


# Defaults
project_id="278964"      # As seen in the projects overview, defaults to https://gitlab.com/gitlab-org/gitlab
since="$(date +%Y-%m-%d)"   # "Only commits after or on this date are returned in ISO 8601 format YYYY-MM-DDTHH:MM:SSZ"
                            # defaults to "now"

# Internals
tmpfile=$(mktemp)
result="start"

main(){
    # Check wether the API token is set and construct a cURL parameter on that basis
    if [[ -n $api_token ]]; then
      # shellcheck disable=2206
      # Create an array with argument + header. We'll check later if $auth_header is set, and if so,
      # include the entire array as an argument for cURL
      auth_header[0]="--header"
      auth_header[1]="PRIVATE-TOKEN: $api_token"
    fi

    page=1

    while [[ "$result" != "" ]]
      do
      # shellcheck disable=SC2086
      # Word splitting is exactly what should be done here with {auth_header[*]} ;)
      result=$(IFS=""; curl -s ${auth_header[0]} ${auth_header[1]} https://gitlab.com/api/v4/projects/$project_id/repository/commits?since=$since\&per_page=100\&page=$page | jq -r ".[] | .author_email" | tee -a "$tmpfile" )
      page=$((page+1))
    done

    echo "Contributors since ${since}: $( ( sort -u | wc -l ) < "$tmpfile")"
}

while getopts :i:t:s: flag
do
  case "${flag}" in
    i) project_id=${OPTARG};;
    t) api_token=${OPTARG};;
    s) since=${OPTARG};;
    *) echo "Usage: $(basename "$0") -i 'project id' [-t 'api token'] [-s YYYY-mm-dd]" >&2
       exit 1 ;;
  esac
done

main