# count-contributors

Count individual contributors for a given timeframe and project

## Usage

### Parameters

| Parameter | Value | Example | Default |
|:-:|:-:|:-:|:-:|
| `-i` | Project ID, listed under the projects name in the overview. | `26595712` | `278964` |
| `-t` | [Personal API token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token) | `aaaaaAaaaaa1a_1Aa-1Aa1` | `null` |
| `-s` | Only commits after or on this date are returned in ISO 8601 format | `2020-01-01` | Now :wink: |